from peewee import Model, MySQLDatabase
from dotenv import load_dotenv
from pathlib import Path

import os

path_env = Path('.') / '.env'
load_dotenv(dotenv_path=path_env)


mysql = MySQLDatabase(
    os.environ['DATABASE_NAME'],
    user=os.environ['DATABASE_USER'],
    password=os.environ['DATABASE_PASS'],
    host=os.environ['DATABASE_HOST'],
    port=int(os.environ['DATABASE_PORT'])
)


class BaseModel(Model):
    class Meta:
        database = mysql
