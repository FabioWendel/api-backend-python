from json import JSONEncoder
from datetime import datetime, timedelta, time, date


class Serialize(JSONEncoder):
    def default(self, field):
        if isinstance(field, datetime):
            return field.isoformat() + 'Z'

        if isinstance(field, timedelta) or isinstance(field, time) or isinstance(field, date):
            return field.isoformat()
