from peewee_validates import ModelValidator
from sanic.request import Request
from sanic.response import json
from src.database.database import mysql
from src.models.testeDeMecha import TesteDeMecha
from playhouse.shortcuts import model_to_dict, update_model_from_dict
from json import dumps
from src.ultis.serialize import Serialize
from peewee import DoesNotExist


class TesteDeMechaController:
    @mysql.atomic()
    async def index(self, request: Request, anamnese_id):
        query = TesteDeMecha.select().where(TesteDeMecha.anamnese == anamnese_id)
        mechas = list()

        for data in query:
            mecha = model_to_dict(data, backrefs=True, recurse=False)
            mechas.append(mecha)
        return json(mechas, dumps=dumps, cls=Serialize)

    @mysql.atomic()
    async def show(self, request: Request, anamnese_id: str, mid: str):
        try:
            mecha = TesteDeMecha.get(id=mid).where(TesteDeMecha.anamnese == anamnese_id)
        except DoesNotExist as e:
            return json({'Error': 'Teste de mecha não encontrado'}, status=404)

        mecha = model_to_dict(mecha, backrefs=True, recurse=True)
        return json(mecha, dumps=dumps, cls=Serialize)

    async def store(self, request: Request, anamnese_id: str):
        with mysql.atomic() as transaction:
            if request.json is None:
                transaction.rollback()
                return json({'Error': 'Campos invalidos'}, status=400)

            mechas = request.json["anamnese"]["testeDeMecha"]
            obj_mechas = list()
            dataMecha = dict()
            for mecha in mechas:
                dataMecha["anamnese"] = anamnese_id
                dataMecha["mecha"] = mecha["mecha"]
                dataMecha["descolorante"] = mecha["descolorante"]
                dataMecha["volumeOX"] = mecha["volumeOX"]
                dataMecha["tempo"] = mecha["tempo"]
                dataMecha["resultado"] = mecha["resultado"]
                dataMecha["procedimentoRealizado"] = mecha["procedimentoRealizado"]
                dataMecha["data"] = mecha["data"]
                obj_mechas.append(TesteDeMecha.create(**dataMecha))
            dict_mechas = []
            for mecha in obj_mechas:
                dict_mechas.append(model_to_dict(mecha, backrefs=True, recurse=False))

            return json(dict_mechas, dumps=dumps, cls=Serialize, status=201)

    @mysql.atomic()
    async def update(self, request: Request, anamnese_id: str):
        print("uau", anamnese_id)
        try:
            print("init")
            mecha = TesteDeMecha.get(id=anamnese_id)
            print(mecha)
        except DoesNotExist as e:
            return json({'Error': 'Teste de Mecha nao encontrado'}, status=404)

        for key, val in request.json.items():
            if key not in mecha.__dict__['__data__']:
                return json({'Error': '{} não pertence ao modelo de mechas'.format(key)}, status=400)
            print(request.json)

        mechas = request.json
        obj_mechas = list()
        dataMecha = dict()
        print("entrei")

        for mecha in mechas:
            print("entrei")
            dataMecha["mecha"] = mecha["mecha"]
            dataMecha["descolorante"] = mecha["descolorante"]
            dataMecha["volumeOX"] = mecha["volumeOX"]
            dataMecha["tempo"] = mecha["tempo"]
            dataMecha["resultado"] = mecha["resultado"]
            dataMecha["procedimentoRealizado"] = mecha["procedimentoRealizado"]
            dataMecha["data"] = mecha["data"]
            obj_mechas.append(TesteDeMecha.update(**dataMecha).where(TesteDeMecha.anamnese == anamnese_id).execute())
        dict_mechas = []
        for mecha in obj_mechas:
            dict_mechas.append(model_to_dict(mecha, backrefs=True, recurse=False))

        # mecha.update(**request.json).execute()
        # mecha = update_model_from_dict(mecha, request.json)
        # mecha = model_to_dict(mecha, backrefs=True, recurse=False)
        return json(dict_mechas, dumps=dumps, cls=Serialize, status=200)

    async def destroy(self, request: Request, mid):
        try:
            mecha = TesteDeMecha.get(id=mid)
        except DoesNotExist as e:
            return json({'Error': 'Produto não encontrado'}, status=404)

        mecha.delete_instance(recursive=True)
        return json({'Delete': mid})
