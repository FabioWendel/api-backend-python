from sanic.request import Request
from sanic.response import json
from peewee_validates import ModelValidator
from src.models.financa import Financa
from playhouse.shortcuts import model_to_dict, update_model_from_dict
from json import dumps
from src.ultis.serialize import Serialize
from peewee import fn, DoesNotExist
from src.database.database import mysql
from datetime import datetime


class FinancaController:
    @mysql.atomic()
    async def index(self, request: Request) -> json:
        query = Financa.select()
        
        result = dict()
        financas = list()
        
        for data in query:
            financa = model_to_dict(data, backrefs=True, recurse=False)
            financas.append(financa)
            
        result['financas'] = financas
        result['entradas'] = sum(financa['preco'] for financa in financas if financa['preco'] >= 0)
        result['saidas'] = sum(abs(financa['preco']) for financa in financas if financa['preco'] < 0)
        result['total'] = result['entradas'] + abs(result['saidas'])
        
        print(result)
        
        return json(result, dumps=dumps, cls=Serialize)

    @mysql.atomic()
    async def show(self, request: Request, fid: str) -> json:
        try:
            financa = Financa.get(id=fid)
            financa = model_to_dict(financa, backrefs=True, recurse=False)
            return json(financa, dumps=dumps, cls=Serialize)
            
        except DoesNotExist as e:
            return json({'erro': 'finança no encontrada'}, status=404)

    async def store(self, request: Request) -> json:
        with mysql.atomic() as transaction:
            if request.json is None:
                return json({'erro': 'campos vazios'}, status=400)
            
            validator = ModelValidator(Financa(**request.json))
            validator.validate()
            
            if bool(validator.errors):
                return json(validator.errors)
            
            financa = Financa.create(**request.json)
            financa = model_to_dict(financa, backrefs=True, recurse=False)
                
            return json(financa, dumps=dumps, cls=Serialize, status=201)

    @mysql.atomic()
    async def update(self, request: Request, fid):
        try:
            financa = Financa.get(id=fid)
        except DoesNotExist as e:
            return json({'error': 'Financa nao encontrado'}, status=404)

        if 'id' in request.json:
            del request.json['id']
            
        for key, val in request.json.items():
            if key not in financa.__dict__['__data__']:
                return json({'error': '{} não pertence ao modelo de financa'.format(key)}, status=400)
        
        financa.updatedAt = datetime.utcnow()
        Financa.update(**request.json).where(Financa.id==fid).execute()
        
        financa = update_model_from_dict(financa, request.json)
        financa = model_to_dict(financa, backrefs=True, recurse=False)
        return json(financa, dumps=dumps, cls=Serialize, status=200)

    async def destroy(self, request: Request, fid):
        try:
            financa = Financa.get(id=fid)
        except DoesNotExist as e:
            return json({'Error': 'Produto não encontrado'}, status=404)

        financa.delete_instance(recursive=True)
        return json({'delete': fid})
