from sanic.request import Request
from sanic.response import json
from src.models.cliente import Cliente
from src.models.anamnese import Anamnese
from src.models.testeDeMecha import TesteDeMecha
from src.controllers.testeDeMecha import TesteDeMechaController
from playhouse.shortcuts import model_to_dict, update_model_from_dict
from json import dumps
from src.ultis.serialize import Serialize
from peewee import DoesNotExist, JOIN
from peewee_validates import ModelValidator
from src.database.database import mysql
from datetime import datetime


class ClienteController:
    @mysql.atomic()
    async def index(self, request: Request):
        query = Cliente.select()
        clientes = list()
        for data in query:
            cliente = model_to_dict(data, backrefs=True, recurse=True)
            cliente["anamnese"] = None if len(cliente["anamnese"]) == 0 else cliente["anamnese"][0]
            clientes.append(cliente)

        return json(clientes, dumps=dumps, cls=Serialize)

    @mysql.atomic()
    async def show(self, request: Request, cid: str):
        try:
            cliente = Cliente.get(id=cid)
        except DoesNotExist as e:
            return json({'Error': 'Cliente não encontrado'}, status=404)

        cliente = model_to_dict(cliente, backrefs=True, recurse=True)
        cliente["anamnese"] = None if len(cliente["anamnese"]) == 0 else cliente["anamnese"][0]
        return json(cliente, dumps=dumps, cls=Serialize)

    async def store(self, request: Request):
        with mysql.atomic() as transaction:
            if request.json is None:
                transaction.rollback()
                return json({'Error': 'Campos invalidos'}, status=400)

            validator = ModelValidator(Cliente(**request.json))
            validator.validate()

            if bool(validator.errors):
                return json(validator.errors, status=400)

            try:
                cliente = Cliente.create(**request.json)
            except Exception as e:
                transaction.rollback()

            dataAnamnese = dict()
            dataAnamnese["cliente"] = cliente.id
            dataAnamnese["dataCriacao"] = datetime.utcnow()

            for key, value in request.json['anamnese'].items():
                dataAnamnese[key] = value

            try:
                anamnese = Anamnese.create(**dataAnamnese)
            except Exception as e:
                transaction.rollback()

            mechas = request.json["anamnese"]["testeDeMecha"]
            obj_mechas = list()
            dataMecha = dict()

            for idx, mecha in enumerate(mechas):
                dataMecha["anamnese"] = anamnese.id
                dataMecha['mecha'] = str(idx + 1)

                for key, data in mecha.items():
                    dataMecha[key] = data

                try:
                    obj = TesteDeMecha.create(**dataMecha)
                except Exception as e:
                    transaction.rollback()

                obj_mechas.append(obj)

            mechas = []
            for mecha in obj_mechas:
                mechas.append(model_to_dict(mecha, backrefs=True, recurse=False))

            cliente = model_to_dict(cliente, backrefs=True, recurse=False)
            cliente["anamnese"] = model_to_dict(anamnese, backrefs=True, recurse=False)
            cliente["anamnese"]['mechas'] = mechas
            print(cliente)

            return json(cliente, dumps=dumps, cls=Serialize, status=201)

    @mysql.atomic()
    async def update(self, request: Request, cid):
        try:
            cliente = Cliente.get(id=cid)
        except DoesNotExist as e:
            return json({'Error': 'Cliente nao encontrado'}, status=404)

        for key, val in request.json.items():
            if key not in cliente.__dict__['__data__'] and key != 'anamnese':
                return json({'Error': '{} não pertence ao modelo de cliente'.format(key)}, status=400)

        dataAnamnese = dict()
        if 'anamnese' in request.json:
            dataAnamnese = request.json['anamnese'].copy()
            del request.json['anamnese']

        dataTesteMecha = list()
        if 'testeDeMecha' in dataAnamnese:
            dataTesteMecha = dataAnamnese['testeDeMecha'].copy()
            del dataAnamnese['testeDeMecha']

        request.json['updatedAt'] = datetime.utcnow()
        cliente.update(**request.json).where(Cliente.id == cid).execute()

        if bool(dataAnamnese):
            dataAnamnese['updatedAt'] = datetime.utcnow()
            Anamnese.update(**dataAnamnese).where(Anamnese.cliente == cid).execute()

        for mecha in dataTesteMecha:
            mecha['updatedAt'] = datetime.utcnow()
            TesteDeMecha.update(**mecha).where(TesteDeMecha.id == mecha['id']).execute()

        cliente = Cliente.get(id=cid)
        cliente = model_to_dict(cliente, backrefs=True, recurse=True)
        return json(cliente, dumps=dumps, cls=Serialize, status=200)

    @mysql.atomic()
    async def destroy(self, request: Request, cid):
        try:
            cliente = Cliente.get(id=cid)
        except DoesNotExist as e:
            return json({'Error': 'Cliente não encontrado'}, status=404)

        cliente.delete_instance(recursive=True)
        return json({'Delete': cid})
