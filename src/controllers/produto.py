from peewee_validates import ModelValidator
from sanic.request import Request
from sanic.response import json
from src.database.database import mysql
from src.models.produto import Produto
from playhouse.shortcuts import model_to_dict, update_model_from_dict
from json import dumps
from src.ultis.serialize import Serialize
from peewee import DoesNotExist
from datetime import datetime


class ProdutoController:
    @mysql.atomic()
    async def index(self, request: Request):
        query = Produto.select()
        produtos = list()

        for data in query:
            produto = model_to_dict(data, backrefs=True, recurse=True)
            produtos.append(produto)
            
        return json(produtos, dumps=dumps, cls=Serialize)
    
    @mysql.atomic()
    async def show(self, request: Request, pid: str):
        try:
            produto = Produto.get(id=pid)
            produto = model_to_dict(produto, backrefs=True, recurse=False)
            return json(produto, dumps=dumps, cls=Serialize)
        
        except DoesNotExist as e:
            return json({'erro': 'Produto nao encontrado'}, status=404)

    @mysql.atomic()
    async def store(self, request: Request):
        with mysql.atomic() as transaction:
            if request.json is None:
                transaction.rollback()
                return json({'Error': 'Campos invalidos'}, status=400)

            validator = ModelValidator(Produto(**request.json))
            validator.validate()

            if bool(validator.errors):
                return json(validator.errors, status=400)
            
            produto = Produto.create(**request.json)        
            produto = model_to_dict(produto, backrefs=True, recurse=False)
            return json(produto, dumps=dumps, cls=Serialize, status=201)

    @mysql.atomic()
    async def update(self, request: Request, pid):
        try:
            produto = Produto.get(id=pid)
        except DoesNotExist as e:
            return json({'error': 'Cliente nao encontrado'}, status=404)

        if 'id' in request.json:
            del request.json['id']
            
        for key, val in request.json.items():
            if key not in produto.__dict__['__data__']:
                return json({'error': '{} não pertence ao modelo de produto'.format(key)}, status=400)
        
        produto.updatedAt = datetime.utcnow()
        Produto.update(**request.json).where(Produto.id==pid).execute()
        produto = update_model_from_dict(produto, request.json)
        produto = model_to_dict(produto, backrefs=True, recurse=False)
        return json(produto, dumps=dumps, cls=Serialize, status=200)

    async def destroy(self, request: Request, pid):
        try:
            produto = Produto.get(id=pid)
        except DoesNotExist as e:
            return json({'Error': 'Produto não encontrado'}, status=404)

        produto.delete_instance(recursive=True)
        return json({'Delete': pid})
