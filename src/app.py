from sanic.request import Request
from sanic.response import json
from sanic import Sanic
from sanic_cors import CORS
from src.routes import routes
from src.database.database import mysql
from src.models.anamnese import Anamnese
from src.models.cliente import Cliente
from src.models.produto import Produto
from src.models.testeDeMecha import TesteDeMecha
from src.models.financa import Financa
from peewee import IntegrityError, ProgrammingError

# pip3 install -r requirement.txt
# pip3 freeze > requirement.txt
# source venv/bin/active
# pem add (patch model)
# pem watch
# pem migrate

app = Sanic(__name__)
CORS(app)
app.blueprint(routes)


@app.listener('after_server_start')
@mysql.atomic()
async def create_tables(server: Sanic, loop):
    print(loop, server)

    try:
        mysql.create_tables([
            Anamnese,
            Cliente,
            Produto,
            TesteDeMecha,
            Financa
        ])

    except IntegrityError as e:
        pass

    except ProgrammingError as e:
        pass
