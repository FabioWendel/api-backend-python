from sanic.request import Request
from sanic.response import json
from sanic import Blueprint
from src.controllers.financa import FinancaController


financa = Blueprint('content_financa', url_prefix='/financa')


@financa.get('/')
async def index(request: Request):
    return await FinancaController().index(request)


@financa.get('/<uid>')
async def show(request: Request, uid: str):
    return await FinancaController().show(request, uid)


@financa.post('/')
async def store(request: Request):
    return await FinancaController().store(request)


@financa.delete('/<uid>')
async def delete(request: Request, uid: str):
    return await FinancaController().destroy(request, uid)


@financa.put('/<uid>')
async def update(request: Request, uid: str):
    return await FinancaController().update(request, uid)


@financa.options('/')
async def options(request: Request):
    return json(None)