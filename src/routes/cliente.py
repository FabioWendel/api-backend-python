from sanic.request import Request
from sanic.response import json
from sanic import Blueprint
from src.controllers.cliente import ClienteController


cliente = Blueprint('content_cliente', url_prefix='/clientes')


@cliente.middleware('request')
async def middleware(request: Request):
    pass


@cliente.get('/')
async def index(request: Request):
    return await ClienteController().index(request)


@cliente.get('/<cid>')
async def show(request: Request, cid: str):
    return await ClienteController().show(request, cid)


@cliente.post('/')
async def store(request: Request):
    return await ClienteController().store(request)


@cliente.put('/<cid>')
async def update(request: Request, cid: str):
    return await ClienteController().update(request, cid)


@cliente.delete('/<cid>')
async def destroy(request: Request, cid: str):
    return await ClienteController().destroy(request, cid)


@cliente.options('/')
async def options(request: Request):
    return json(None)
