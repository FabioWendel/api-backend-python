from sanic import Blueprint
from .cliente import cliente
from .produto import produto
from .testeDeMecha import mecha
from .financa import financa


routes = Blueprint.group([cliente, produto, mecha, financa])
