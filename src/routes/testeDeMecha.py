from sanic.request import Request
from sanic.response import json
from sanic import Blueprint
from src.controllers.testeDeMecha import TesteDeMechaController


mecha = Blueprint('content_mechas', url_prefix='/anamnese/<anamnese_id>/mechas')


@mecha.middleware('request')
async def middleware(request: Request):
    pass


@mecha.get('/')
async def index(request: Request, anamnese_id: str):
    return await TesteDeMechaController().index(request, anamnese_id)


@mecha.get('/<mid>')
async def show(request: Request, anamnese_id: str, mid: str):
    return await TesteDeMechaController().show(request, anamnese_id, mid)


@mecha.post('/')
async def store(request: Request, anamnese_id: str):
    return await TesteDeMechaController().store(request, anamnese_id)


@mecha.put('/<mid>')
async def update(request: Request, anamnese_id: str):
    return await TesteDeMechaController().update(request, anamnese_id)


@mecha.delete('/<mid>')
async def destroy(request: Request, mid: str):
    return await TesteDeMechaController().destroy(request, mid)


@mecha.options('/')
async def options(request: Request):
    return json(None)
