from sanic.request import Request
from sanic.response import json
from sanic import Blueprint
from src.controllers.produto import ProdutoController


produto = Blueprint('content_produto', url_prefix='/produtos')


@produto.middleware('request')
async def middleware(request: Request):
    pass


@produto.get('/')
async def index(request: Request):
    return await ProdutoController().index(request)


@produto.get('/<pid>')
async def show(request: Request, pid: str):
    return await ProdutoController().show(request, pid)


@produto.post('/')
async def store(request: Request):
    return await ProdutoController().store(request)


@produto.put('/<pid>')
async def update(request: Request, pid: str):
    return await ProdutoController().update(request, pid)


@produto.delete('/<pid>')
async def destroy(request: Request, pid: str):
    return await ProdutoController().destroy(request, pid)


@produto.options('/')
async def options(request: Request):
    return json(None)
