from src.database.database import BaseModel
from datetime import datetime

import peewee


class Produto(BaseModel):
    nome = peewee.CharField(max_length=150)
    codBarra = peewee.IntegerField(unique=True)
    precoUnitario = peewee.IntegerField()
    qtd = peewee.IntegerField()
    
    createdAt = peewee.DateTimeField(default=datetime.utcnow())
    updatedAt = peewee.DateTimeField(default=datetime.utcnow())
