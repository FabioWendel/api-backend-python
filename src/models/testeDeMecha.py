from src.database.database import BaseModel
from src.models.anamnese import Anamnese
from datetime import datetime

import peewee


class TesteDeMecha(BaseModel):
    anamnese = peewee.ForeignKeyField(Anamnese, backref='testeDeMecha')
    mecha = peewee.TextField()
    descolorante = peewee.TextField()
    volumeOX = peewee.TextField()
    tempo = peewee.TextField()
    resultado = peewee.TextField()
    procedimentoRealizado = peewee.TextField()
    data = peewee.DateTimeField()

    createdAt = peewee.DateTimeField(default=datetime.utcnow())
    updatedAt = peewee.DateTimeField(default=datetime.utcnow())
