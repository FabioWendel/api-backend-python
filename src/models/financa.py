from src.database.database import BaseModel
from src.models.cliente import Cliente
from datetime import datetime

import peewee


class Financa(BaseModel):
    titulo = peewee.TextField()
    preco = peewee.FloatField()
    categoria = peewee.CharField()
    entradas = peewee.VirtualField()
    saidas = peewee.VirtualField()
    total = peewee.VirtualField()
    
    createdAt = peewee.DateTimeField(default=datetime.utcnow())
    updatedAt = peewee.DateTimeField(default=datetime.utcnow())