from src.database.database import BaseModel
from datetime import datetime

import peewee


class Cliente(BaseModel):
    nome = peewee.CharField(max_length=150)
    numero = peewee.CharField(max_length=150)
    cpf = peewee.CharField(max_length=150, unique=True)
    email = peewee.CharField(max_length=150, unique=True)
    nascimento = peewee.DateTimeField()

    createdAt = peewee.DateTimeField(default=datetime.utcnow())
    updatedAt = peewee.DateTimeField(default=datetime.utcnow())
