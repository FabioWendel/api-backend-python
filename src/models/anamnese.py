from src.database.database import BaseModel
from src.models.cliente import Cliente
from datetime import datetime

import peewee


class Anamnese(BaseModel):
        cliente = peewee.ForeignKeyField(Cliente, backref='anamnese')
        dataCriacao = peewee.DateTimeField()
        p3Meses = peewee.BooleanField(default=False)
        p3MesesEsp = peewee.TextField(null=True)
        frequenciaHR = peewee.BooleanField(default=False)
        frequenciaHREsp = peewee.TextField(null=True)
        frenquenciaSecador = peewee.BooleanField(default=False)
        gestante = peewee.BooleanField(default=False)
        gestanteSemana = peewee.TextField(null=True)
        alergia = peewee.BooleanField(default=False)
        alergiaEsp = peewee.TextField(null=True)
        queda = peewee.BooleanField(default=False)
        seborreia = peewee.BooleanField(default=False)
        dermatite = peewee.BooleanField(default=False)
        tricoptilose = peewee.BooleanField(default=False)
        tricotilomania = peewee.BooleanField(default=False)
        pediculose = peewee.BooleanField(default=False)
        triconodose = peewee.BooleanField(default=False)
        tricorrexi = peewee.BooleanField(default=False)
        procedimento = peewee.TextField(null=True)
        tipoCabelo = peewee.TextField(null=True)
        tipoCabelo2 = peewee.TextField(null=True)
        corNaturalCabelo = peewee.BooleanField(default=False)
        corNaturalEsp = peewee.TextField(null=True)
        caracteristicaCabelo = peewee.TextField(null=True)
        densidade = peewee.TextField(null=True)
        textura = peewee.TextField(null=True)

        createdAt = peewee.DateTimeField(default=datetime.utcnow())
        updatedAt = peewee.DateTimeField(default=datetime.utcnow())
