# auto-generated snapshot
from peewee import *
import datetime
import peewee


snapshot = Snapshot()


@snapshot.append
class Cliente(peewee.Model):
    nome = CharField(max_length=150)
    numero = CharField(max_length=150)
    cpf = CharField(max_length=150, unique=True)
    email = CharField(max_length=150, unique=True)
    nascimento = DateTimeField()
    class Meta:
        table_name = "cliente"


@snapshot.append
class Anamnese(peewee.Model):
    cliente = snapshot.ForeignKeyField(backref='anamnese', index=True, model='cliente')
    dataCriacao = DateTimeField()
    p3Meses = BooleanField(default=False)
    p3MesesEsp = TextField(null=True)
    frequenciaHR = BooleanField(default=False)
    frequenciaHREsp = TextField(null=True)
    frenquenciaSecador = BooleanField(default=False)
    gestante = BooleanField(default=False)
    gestanteSemana = TextField(null=True)
    alergia = BooleanField(default=False)
    alergiaEsp = TextField(null=True)
    queda = BooleanField(default=False)
    seborreia = BooleanField(default=False)
    dermatite = BooleanField(default=False)
    tricoptilose = BooleanField(default=False)
    tricotilomania = BooleanField(default=False)
    pediculose = BooleanField(default=False)
    triconodose = BooleanField(default=False)
    tricorrexi = BooleanField(default=False)
    procedimento = TextField(null=True)
    tipoCabelo = TextField(null=True)
    tipoCabelo2 = TextField(null=True)
    corNaturalCabelo = BooleanField(default=False)
    corNaturalEsp = TextField(null=True)
    caracteristicaCabelo = TextField(null=True)
    densidade = TextField(null=True)
    textura = TextField(null=True)
    class Meta:
        table_name = "anamnese"


@snapshot.append
class Produto(peewee.Model):
    nome = CharField(max_length=150)
    codBarra = IntegerField(unique=True)
    precoUnitario = IntegerField()
    qtd = IntegerField()
    class Meta:
        table_name = "produto"


@snapshot.append
class TesteDeMecha(peewee.Model):
    anamnese = snapshot.ForeignKeyField(backref='testeDeMecha', index=True, model='anamnese')
    mecha = TextField()
    descolorante = TextField()
    volumeOX = TextField()
    tempo = TextField()
    resultado = TextField()
    procedimentoRealizado = TextField()
    data = DateTimeField()
    class Meta:
        table_name = "testedemecha"


