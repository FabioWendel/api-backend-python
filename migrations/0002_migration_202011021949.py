# auto-generated snapshot
from peewee import *
import datetime
import peewee


snapshot = Snapshot()


@snapshot.append
class Cliente(peewee.Model):
    nome = CharField(max_length=150)
    numero = CharField(max_length=150)
    cpf = CharField(max_length=150, unique=True)
    email = CharField(max_length=150, unique=True)
    nascimento = DateTimeField()
    createdAt = DateTimeField(default=datetime.datetime(2020, 11, 2, 23, 49, 5, 615905))
    updatedAt = DateTimeField(default=datetime.datetime(2020, 11, 2, 23, 49, 5, 615920))
    class Meta:
        table_name = "cliente"


@snapshot.append
class Anamnese(peewee.Model):
    cliente = snapshot.ForeignKeyField(backref='anamnese', index=True, model='cliente')
    dataCriacao = DateTimeField()
    p3Meses = BooleanField(default=False)
    p3MesesEsp = TextField(null=True)
    frequenciaHR = BooleanField(default=False)
    frequenciaHREsp = TextField(null=True)
    frenquenciaSecador = BooleanField(default=False)
    gestante = BooleanField(default=False)
    gestanteSemana = TextField(null=True)
    alergia = BooleanField(default=False)
    alergiaEsp = TextField(null=True)
    queda = BooleanField(default=False)
    seborreia = BooleanField(default=False)
    dermatite = BooleanField(default=False)
    tricoptilose = BooleanField(default=False)
    tricotilomania = BooleanField(default=False)
    pediculose = BooleanField(default=False)
    triconodose = BooleanField(default=False)
    tricorrexi = BooleanField(default=False)
    procedimento = TextField(null=True)
    tipoCabelo = TextField(null=True)
    tipoCabelo2 = TextField(null=True)
    corNaturalCabelo = BooleanField(default=False)
    corNaturalEsp = TextField(null=True)
    caracteristicaCabelo = TextField(null=True)
    densidade = TextField(null=True)
    textura = TextField(null=True)
    createdAt = DateTimeField(default=datetime.datetime(2020, 11, 2, 23, 49, 5, 616724))
    updatedAt = DateTimeField(default=datetime.datetime(2020, 11, 2, 23, 49, 5, 616734))
    class Meta:
        table_name = "anamnese"


@snapshot.append
class Produto(peewee.Model):
    nome = CharField(max_length=150)
    codBarra = IntegerField(unique=True)
    precoUnitario = IntegerField()
    qtd = IntegerField()
    createdAt = DateTimeField(default=datetime.datetime(2020, 11, 2, 23, 49, 5, 618362))
    updatedAt = DateTimeField(default=datetime.datetime(2020, 11, 2, 23, 49, 5, 618372))
    class Meta:
        table_name = "produto"


@snapshot.append
class TesteDeMecha(peewee.Model):
    anamnese = snapshot.ForeignKeyField(backref='testeDeMecha', index=True, model='anamnese')
    mecha = TextField()
    descolorante = TextField()
    volumeOX = TextField()
    tempo = TextField()
    resultado = TextField()
    procedimentoRealizado = TextField()
    data = DateTimeField()
    createdAt = DateTimeField(default=datetime.datetime(2020, 11, 2, 23, 49, 5, 617742))
    updatedAt = DateTimeField(default=datetime.datetime(2020, 11, 2, 23, 49, 5, 617752))
    class Meta:
        table_name = "testedemecha"


def forward(old_orm, new_orm):
    cliente = new_orm['cliente']
    anamnese = new_orm['anamnese']
    produto = new_orm['produto']
    testedemecha = new_orm['testedemecha']
    return [
        # Apply default value datetime.datetime(2020, 11, 2, 23, 49, 5, 615905) to the field cliente.createdAt
        cliente.update({cliente.createdAt: datetime.datetime(2020, 11, 2, 23, 49, 5, 615905)}).where(cliente.createdAt.is_null(True)),
        # Apply default value datetime.datetime(2020, 11, 2, 23, 49, 5, 615920) to the field cliente.updatedAt
        cliente.update({cliente.updatedAt: datetime.datetime(2020, 11, 2, 23, 49, 5, 615920)}).where(cliente.updatedAt.is_null(True)),
        # Apply default value datetime.datetime(2020, 11, 2, 23, 49, 5, 616724) to the field anamnese.createdAt
        anamnese.update({anamnese.createdAt: datetime.datetime(2020, 11, 2, 23, 49, 5, 616724)}).where(anamnese.createdAt.is_null(True)),
        # Apply default value datetime.datetime(2020, 11, 2, 23, 49, 5, 616734) to the field anamnese.updatedAt
        anamnese.update({anamnese.updatedAt: datetime.datetime(2020, 11, 2, 23, 49, 5, 616734)}).where(anamnese.updatedAt.is_null(True)),
        # Apply default value datetime.datetime(2020, 11, 2, 23, 49, 5, 618362) to the field produto.createdAt
        produto.update({produto.createdAt: datetime.datetime(2020, 11, 2, 23, 49, 5, 618362)}).where(produto.createdAt.is_null(True)),
        # Apply default value datetime.datetime(2020, 11, 2, 23, 49, 5, 618372) to the field produto.updatedAt
        produto.update({produto.updatedAt: datetime.datetime(2020, 11, 2, 23, 49, 5, 618372)}).where(produto.updatedAt.is_null(True)),
        # Apply default value datetime.datetime(2020, 11, 2, 23, 49, 5, 617742) to the field testedemecha.createdAt
        testedemecha.update({testedemecha.createdAt: datetime.datetime(2020, 11, 2, 23, 49, 5, 617742)}).where(testedemecha.createdAt.is_null(True)),
        # Apply default value datetime.datetime(2020, 11, 2, 23, 49, 5, 617752) to the field testedemecha.updatedAt
        testedemecha.update({testedemecha.updatedAt: datetime.datetime(2020, 11, 2, 23, 49, 5, 617752)}).where(testedemecha.updatedAt.is_null(True)),
    ]
